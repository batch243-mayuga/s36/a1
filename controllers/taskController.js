const Task = require("../models/task");


module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		status: requestBody.status,
		name: requestBody.name
	})

	return newTask.save().then((task,error) => {

		if(error){
			console.log(error);

			return false;
		} else {

			return task;
		}
	})
}





module.exports.getSpecificTask = (taskId) => {

	return Task.findById(taskId).then((result => {
		return result
	})
	)
}



module.exports.updateTask = (taskId, newContent) => {

			return Task.findByIdAndUpdate(taskId).then((result, error) => {
				if(error){
					console.log(error);
					return false;
				}
				

				return result.save().then((updatedTask, saveErr) => {
					
					if(saveErr){
						console.log(saveErr)
						return false;
					} else{

						return updatedTask
					}
					
				})
			})
		}