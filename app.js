const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3001;

const taskRoute = require("./routes/taskRoute")

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/tasks", taskRoute)

mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.cyvdebm.mongodb.net/activity36?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
);


app.listen(port, () => console.log(`Server running at port ${port}`));